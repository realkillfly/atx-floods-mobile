import { Component, OnInit, ViewChild } from '@angular/core';
import { KillflyFloodDataService } from '../services/killfly-flood-data.service';
import { ApiResponse, Marker } from '../services/apiResponse';
import { AgmMarker, AgmMap } from '@agm/core';



@Component({
  selector: 'app-gate-list',
  templateUrl: './gate-list.component.html',
  styleUrls: ['./gate-list.component.css']
})
export class GateListComponent implements OnInit {

  apiResponse: ApiResponse;// = {"markers":[{"name":"DACY LN (CR 205) - .75 MI S OF WINDY HILL RD (CR 131)","jurisdiction":"HCO","address":"Hays County","lat":30.024374,"lng":-97.831001,"type":"on","comment":"Crossing is closed ","id":6557},{"name":"Blue Hole Park Rd West","jurisdiction":"GEO","address":"City of Georgetown","lat":30.642031,"lng":-97.682266,"type":"on","comment":"","id":7299},{"name":"BELL SPRINGS RD (CR 169) - 1 MI S OF FITZHUGH RD (CR 101)","jurisdiction":"HCO","address":"Hays County","lat":30.244528,"lng":-98.122566,"type":"on","comment":"","id":6609},{"name":"2900-blk Bee Caves Rd","jurisdiction":"TCO","address":"Travis County, TX","lat":30.270645,"lng":-97.791901,"type":"on","comment":"Roadway open","id":6897},{"name":"5000 BLOCK CR 254","jurisdiction":"BURCO","address":"","lat":30.7899072925,"lng":-98.1086553833,"type":"on","comment":"","id":8345},{"name":"CREEK RD (CR 190) - .5 MI E OF MT GAINOR RD (CR 220)","jurisdiction":"HCO","address":"Hays County","lat":30.187622,"lng":-98.115501,"type":"on","comment":"crossing is open","id":6591},{"name":"Gregg Ln @ fm 973","jurisdiction":"TCO","address":"Travis County, TX ","lat":30.375914,"lng":-97.529198,"type":"on","comment":"","id":7548},{"name":"1400 Blk Nature Heights Dr","jurisdiction":"MBF","address":"Between US-281 & Commerce St., Marble Falls","lat":30.599077,"lng":-98.268562,"type":"on","comment":"CROSSING IS OPEN","id":6422},{"name":"2500 BLOCK CR 219","jurisdiction":"BURCO","address":"","lat":30.9229617263,"lng":-97.8616692408,"type":"on","comment":"","id":8373},{"name":"RR 2241 @ Wright's Creek","jurisdiction":"LCO","address":"1620 RR 2241 Llano, TX 78643","lat":30.774242,"lng":-98.62967,"type":"on","comment":"","id":8145},{"name":"20700 Cameron Rd","jurisdiction":"TCO","address":"Travis County, TX","lat":30.457598,"lng":-97.492355,"type":"on","comment":"Roadway open","id":6866},{"name":"Jesse Bohls Rd @ Tributary to Wilbarger Creek","jurisdiction":"TCO","address":"Travis County, TX","lat":30.433605,"lng":-97.558022,"type":"on","comment":"Roadway opened ","id":6185},{"name":"Ivy Switch at County Line","jurisdiction":"CCO","address":"Ivy Switch at County Line","lat":29.664328,"lng":-97.536613,"type":"on","comment":"","id":7969},{"name":"Creek Rd and Mt Gainor Rd","jurisdiction":"HCO","address":"Dripping Springs","lat":30.187366,"lng":-98.123751,"type":"on","comment":"open 01/05/2019","id":8379},{"name":"1500 Blk 2nd St (WESTSIDE PARK)","jurisdiction":"MBF","address":"Between Ave. P & Ave. N., Marble Falls, TX","lat":30.574352,"lng":-98.284576,"type":"on","comment":"Crossing is OPEN  ","id":6429},{"name":"Low Water Crossing #3","jurisdiction":"COA","address":"6650 Spicewood Springs Rd, Austin, TX","lat":30.397104,"lng":-97.7789,"type":"on","comment":"","id":6144},{"name":"Turkey Ridge Rd Riddle Rd","jurisdiction":"BCO","address":"Turkey Ridge Rd Riddle Rd","lat":30.015995,"lng":-97.539978,"type":"on","comment":"Roadway is open","id":7589},{"name":"FM 150 @ Moss Rose ln ","jurisdiction":"HCO","address":"Hays County ","lat":30.057116,"lng":-97.989655,"type":"on","comment":"Crossing is open","id":7320},{"name":"E Morrow St at Chamber Way","jurisdiction":"GEO","address":"City of Georgetown","lat":30.648214,"lng":-97.673347,"type":"on","comment":"","id":7261},{"name":"Weiss Ln @ Hodde Ln","jurisdiction":"TCO","address":"Travis County, TX","lat":30.457186,"lng":-97.554784,"type":"on","comment":"Roadway is open","id":8378},{"name":"Barth Rd/FM 672","jurisdiction":"CCO","address":"Barth Rd/FM 672","lat":29.927589,"lng":-97.595444,"type":"on","comment":"","id":7831},{"name":"1700 Blk US Hwy 281 Bridge","jurisdiction":"MBF","address":"Between Marble Hts. & Latana Dr.","lat":30.588221,"lng":-98.273964,"type":"on","comment":"Crossing is OPEN","id":6437}],"numberGatesTotal":2320,"numberGatesOpen":2212,"numberGatesClosed":108,"message":null,"dateLastQuery":"2019-05-10T15:50:19.3585264+00:00"};
  isLoadingData: boolean = false;
  markers: Marker[] = [];

/*
docs:
https://angular-maps.com/api-docs/agm-core/index.html
https://alligator.io/angular/angular-google-maps/
*/


  latitude = 30.2672;
  longitude = -97.7431;

  mapType = 'roadmap';

  constructor(
    private killflyFloodDataService: KillflyFloodDataService
  ) { }

  ngOnInit() {
    this.loadData();
  }

  loadData(){
    this.isLoadingData = true;

    this.killflyFloodDataService.getFloodData()
      .subscribe(resp => {
          this.apiResponse = resp;
          this.displayData(200);
          this.isLoadingData = false;
      })
  }

  displayData(numberToDisplay: number)
  {
    
    this.markers = this.apiResponse.markers.slice(0, numberToDisplay);
  }

  selectMarker($event){

  }

  getFillColor(marker)
  {
    if(marker.type == "on")
    {
      return "green";
    }
    else{
      return "red";
    }
  }

}
