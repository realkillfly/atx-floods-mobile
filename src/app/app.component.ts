import { Component } from '@angular/core';



@Component({
  selector: 'app-root',
  template: `
    <div style="text-align:center">
      <h1>
        {{title}}
      </h1>
    </div>

    <div>
    <app-gate-list></app-gate-list>
    </div>
    <router-outlet></router-outlet>
  `,
  styles: []
})
export class AppComponent {

  title = 'ATX Floods UI - Texrain';

  constructor(){

  }

}
