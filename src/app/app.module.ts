import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AgmCoreModule } from '@agm/core';

import { HttpClientModule } from '@angular/common/http';

import { GateListComponent } from './gate-list/gate-list.component';



@NgModule({
  declarations: [
    AppComponent,
    GateListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBT2RbNVkRokx7fmT8ZVNZUAuad6lEDJIc'
      /* apiKey is required, unless you are a 
      premium customer, in which case you can 
      use clientId 
      */
    })
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
