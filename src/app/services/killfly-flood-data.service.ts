import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';

import {ApiResponse} from './apiResponse';

@Injectable({
  providedIn: 'root'
})
export class KillflyFloodDataService {
  apiEndpoint: string = environment.APIEndpoint;

  constructor(private httpClient: HttpClient) { }

  public getFloodData(){
    return this.httpClient.get<ApiResponse>(this.apiEndpoint + '/api/atxFloods/');
  }
}
