

    export class Marker {
        name: string;
        jurisdiction: string;
        address: string;
        lat: number;
        lng: number;
        type: string;
        comment: string;
        id: number;
    }

    export class ApiResponse {
        markers: Marker[];
        numberGatesTotal: number;
        numberGatesOpen: number;
        numberGatesClosed: number;
        message?: any;
        dateLastQuery: Date;
    }



