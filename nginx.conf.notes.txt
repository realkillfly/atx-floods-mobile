#################   Redirect to HTTPS ########################

worker_processes  1;

events {
    worker_connections  1024;
}

http {

    server {
        listen 80;
        server_name atxfloods.killfly.com www.atxfloods.killfly.com;

        # redirects both www and non-www to ssl port with http (NOT HTTPS, forcing error 497)
        return 301 https://atxfloods.killfly.com$request_uri;
    }

    server {
        listen 443 ssl http2;
        server_name www.atxfloods.killfly.com;

        # redirects www to non-www. wasn't work for me without this server block
        return 301 $scheme://atxfloods.killfly.com$request_uri;
    }

    server {
        listen 443 ssl http2 default_server;
        listen [::]:443 ssl http2 default_server;
        server_name atxfloods.killfly.com;

        root   /usr/share/nginx/html;
        index  index.html index.htm;
        include /etc/nginx/mime.types;

        gzip on;
        gzip_min_length 1000;
        gzip_proxied expired no-cache no-store private auth;
        gzip_types text/plain text/css application/json application/javascript application/x-javascript text/xml application/xml application/xml+rss text/javascript;

        location / {
            try_files $uri $uri/ /index.html;
        }
    

    }
}



################## SIMPLE #############################
worker_processes  1;

events {
    worker_connections  1024;
}

http {

    server {
        listen 80  default_server;
        server_name atxfloods.killfly.com;

        root   /usr/share/nginx/html;
        index  index.html index.htm;
        include /etc/nginx/mime.types;

        gzip on;
        gzip_min_length 1000;
        gzip_proxied expired no-cache no-store private auth;
        gzip_types text/plain text/css application/json application/javascript application/x-javascript text/xml application/xml application/xml+rss text/javascript;

        location / {
            try_files $uri $uri/ /index.html;
        }
    

    }
}